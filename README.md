# IP Finder
A Small, zero-conf utility to help find the IP addresses of other computers on the local network.

## How it works
IP Finder can discover the IP and hostnames of all devices on the LAN that are also running this program. It is very small, at around 20KB.

## Usage
To use this program, simply launch it on all the computers that you want to find the IPs of.

This program was designed with LAN parties in mind, so running it on all the computers that you are playing games on, and then copying the IP addresses out, rather than shouting across rooms.