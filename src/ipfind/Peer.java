/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfind;

/**
 *
 * @author znix
 */
public class Peer {
    public final String addr;
    public final String name;

    public Peer(String addr, String name) {
        this.addr = addr;
        this.name = name;
    }

    @Override
    public String toString() {
        return addr + ": " + name;
    }
}
