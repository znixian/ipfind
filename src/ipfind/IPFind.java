/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfind;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

/**
 *
 * @author znix
 */
public final class IPFind {

    // Which port
    private static final int PORT = 3981;
    // Which address
    private static final String GROUP_NAME = "224.2.137.121";
    // Which ttl
    private static final int TTL = 1;

    private static final int CODE_ANNOUNCE = 1;
    private static final int CODE_SCAN = 2;
    private static final int CODE_LEAVE = 3;

    private final UI ui;
    private final MulticastSocket s;
    private final InetAddress group;

    private final Map<InetAddress, String> peers;
    private DefaultListModel<Peer> peerList;

    private boolean isClosing;

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        IPFind ipfind = new IPFind();
    }

    private IPFind() throws IOException {
        peers = new HashMap<>();

        ui = new UI(this);
        peerList = new DefaultListModel<>();
        ui.getPeerList().setModel(peerList);

        ui.setVisible(true);

        // Create the socket but we don't bind it as we are only going to send data
        s = new MulticastSocket(PORT);
        group = InetAddress.getByName(GROUP_NAME);
        s.joinGroup(group);

        refreshPressed();

        try {
            for (;;) {
                run();
            }
        } catch (IOException ex) {
            Logger.getLogger(IPFind.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void announce() {
        String compname = ui.getCompname();
        byte[] toSend = new byte[compname.length() + 1];
        toSend[0] = CODE_ANNOUNCE;
        System.arraycopy(compname.getBytes(), 0, toSend, 1, compname.length());
        sendPacket(toSend);
    }

    public void refreshPressed() {
        peers.clear();
        byte buf[] = new byte[]{CODE_SCAN};
        sendPacket(buf);
    }

    private void sendPacket(byte buf[]) {
        try {
            // Create a DatagramPacket
            DatagramPacket pack = new DatagramPacket(buf, buf.length, group, PORT);

            // Do a send. Note that send takes a byte for the ttl and not an int.
            s.send(pack, (byte) TTL);
        } catch (IOException ex) {
            Logger.getLogger(IPFind.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void close() {
        try {
            byte buf[] = new byte[]{CODE_LEAVE};
            sendPacket(buf);

            s.leaveGroup(InetAddress.getByName(GROUP_NAME));
            s.close();
        } catch (IOException ex) {
            Logger.getLogger(IPFind.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void run() throws IOException {
        byte buf[] = new byte[1024];
        DatagramPacket pack = new DatagramPacket(buf, buf.length);
        s.receive(pack);

        if (pack.getLength() == 0) {
            return;
        }

        if (buf[0] == CODE_ANNOUNCE) {
            String str = new String(buf, 1, pack.getLength());
            peers.put(pack.getAddress(), str);
            refresh();
        } else if (buf[0] == CODE_LEAVE) {
            peers.remove(pack.getAddress());
            refresh();
        } else if (buf[0] == CODE_SCAN) {
            announce();
        }
    }

    public DefaultListModel<Peer> getPeerList() {
        return peerList;
    }

    private void refresh() {
        peerList.clear();
        peers.entrySet().stream().forEach((peer) -> {
            peerList.addElement(new Peer(peer.getKey().getHostAddress(), peer.getValue()));
        });
    }
}
